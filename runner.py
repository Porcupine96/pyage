# pylint: disable=all

from subprocess import call

def run():
    base = ['python', '-m', 'pyage.core.bootstrap', 'pyage.tsp.conf']

    generate_cities = 0 # False
    agents_count = 5

    for emas in [0, 1]:
       for mutation_prob in [0.01, 0.05, 0.1]:
           for mutation_type in ['random_swap', 'neighbour_swap']:
                retry_count = 5
                options = map(str, [emas, generate_cities, mutation_prob, mutation_type, agents_count])

                for i in range(retry_count):
                    print('Running iteration: {0}'.format(i))
                    id = '{0}-{1}-{2}-{3}-{4}'.format(i, emas, mutation_prob, mutation_type, agents_count)
                    call(base + options + [id])

if __name__ == '__main__':
    run()
