# pylint: disable=invalid-name,redefined-outer-name,line-too-long

import csv
from matplotlib import pyplot as plt

def plot_cities(cities, canvas):
    for x, y in cities:
        canvas.plot(x, y, 'ro')

def plot_paths(cities, canvas):
    size = len(cities)
    x1, y1 = cities[0]
    for i in range(size):
        x2, y2 = cities[(i+1) % size]
        canvas.plot([x1, x2], [y1, y2], color='b', linestyle='-', linewidth=0.5)
        x1, y1 = x2, y2

def draw_graph(cities):
    figure = plt.figure()
    canvas = figure.add_subplot(111)
    plot_paths(cities, canvas)
    plot_cities(cities, canvas)
    plt.draw()
    plt.savefig("results.png")

def read_cities(filename='cities.csv'):
    cities = []
    with open(filename, "r") as file:
        file.readline()
        reader = csv.reader(file, delimiter=',')
        for row in reader:
            x, y = row
            cities.append((int(x), int(y)))
    return cities

def draw(genotype):
    cities = read_cities()
    ordered_cities = [cities[i] for i in genotype]
    draw_graph(ordered_cities)

if __name__ == "__main__":
    genotype = [16,8,4,15,19,2,5,17,6,12,18,7,13,9,11,14,3,0,10,1]
    draw(genotype)
