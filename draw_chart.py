def read_results():
    results = []
    for filename in os.listdir('.'):
        if filename.startswith('fitness_'):
            with open(filename, 'r') as file:
                reader = csv.reader(file, delimiter=';')
                for iter_count, fitness in reader:
                    results.append((iter_count, fitness))
    return results
