run: clean
	#python -m pyage.core.bootstrap pyage.tsp.conf 'DEBUG'
	python runner.py

draw:
	python draw_path.py
	open results.png

clean:
	rm -f *.txt *.log
