# pylint: disable=invalid-name,too-few-public-methods,line-too-long

class City(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

class VisitOrder(object):
    def __init__(self, cities, order):
        self.cities = cities
        self.order = order
        self.size = len(cities)
        self.fitness = self.calculate_fitness()

    def calculate_fitness(self):
        return - sum([VisitOrder.__city_distance(self.__city_at(i), self.__city_at(i + 1)) for i in range(self.size - 1)]) \
               - VisitOrder.__city_distance(self.__city_at(0), self.__city_at(self.size - 1))

    def __city_at(self, index):
        return self.cities[self.order[index]]

    @staticmethod
    def __city_distance(first, second):
        return (first.x - second.x) ** 2 + (first.y - second.y) ** 2

    def __str__(self):
        return 'VisitOrder {0}'.format(','.join(map(str, self.order)))
