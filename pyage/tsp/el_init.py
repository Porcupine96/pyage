# pylint: disable=redefined-builtin,no-member,too-few-public-methods

import random
from pyage.core.emas import EmasAgent
from pyage.core.operator import Operator
from pyage.tsp.el_genotype import VisitOrder
from pyage.core.inject import Inject

class EmasInitializer(object):

    def __init__(self, cities, energy, size):
        self.cities = cities
        self.energy = energy
        self.size = size

    @Inject("naming_service")
    def __call__(self):
        agents = {}
        for _ in range(self.size):
            city_order = list(range(len(self.cities)))
            random.shuffle(city_order)
            agent = EmasAgent(
                VisitOrder(self.cities, city_order),
                self.energy,
                self.naming_service.get_next_agent()
            )
            agents[agent.get_address()] = agent
        return agents


class SimpleInitializer(Operator):
    def __init__(self, cities, population_size):
        super(SimpleInitializer, self).__init__(VisitOrder)
        self.cities = cities
        self.population_size = population_size
        self.population = self.__create_population()

    def __create_population(self):
        population = []
        for _ in range(self.population_size):
            city_order = list(range(len(self.cities)))
            random.shuffle(city_order)
            population.append(VisitOrder(self.cities, city_order))
        return population

    def process(self, population):
        for i in range(self.population_size):
            population.append(self.population[i])

    def __call__(self):
        return self.population


def root_agents_factory(count, type):
    def factory():
        agents = {}
        for i in range(count):
            agent = type('R' + str(i))
            agents[agent.get_address()] = agent
        return agents
    return factory
