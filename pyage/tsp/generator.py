# pylint: disable=redefined-outer-name,invalid-name
import sys
import csv
from random import randint

def generate_cities(count, bound=1000):
    return [(randint(0, bound), randint(0, bound)) for _ in range(count)]


def save_to_csv(cities, filename='cities.csv'):
    with open(filename, 'w') as file:
        writer = csv.writer(file, delimiter=',')
        writer.writerow(['x', 'y'])
        for x, y in cities:
            writer.writerow([x, y])


if __name__ == '__main__':
    count = int(sys.argv[1])
    cities = generate_cities(count)
    save_to_csv(cities)
