# pylint: disable=invalid-name, logging-format-interpolation, redefined-builtin

import logging
import random

from random import randint
from pyage.core.operator import Operator
from pyage.tsp.el_genotype import VisitOrder

logger = logging.getLogger(__name__)

class AbstractMutation(Operator):
    def __init__(self, type, probability):
        super(AbstractMutation, self).__init__()
        self.probability = probability

    def mutate(self, genotype):
        pass

    def process(self, population):
        for genotype in population:
            if random.random() < self.probability:
                self.mutate(genotype)

    @staticmethod
    def _swap(index_one, index_two, array):
        tmp = array[index_one]
        array[index_one] = array[index_two]
        array[index_two] = tmp
        return array

class RandomSwap(AbstractMutation):
    def __init__(self, probability):
        super(RandomSwap, self).__init__(VisitOrder, probability)

    def mutate(self, genotype):
        logger.debug('Mutating genotype: {0}'.format(genotype))

        first = randint(0, genotype.size-1)
        second = randint(0, genotype.size-1)
        old_order = genotype.order[:]
        new_order = AbstractMutation._swap(first, second, old_order)

        logger.debug('Swapping: %s and %s, result %s', first, second, new_order)

        return VisitOrder(genotype.cities, new_order)


class NeighbourSwap(AbstractMutation):
    def __init__(self, probability):
        super(NeighbourSwap, self).__init__(VisitOrder, probability)

    def mutate(self, genotype):
        logger.debug('Mutating genotype: {0}'.format(genotype))

        first = randint(0, genotype.size-1)
        second = (first+1) % genotype.size
        old_order = genotype.order[:]
        new_order = AbstractMutation._swap(first, second, old_order)

        logger.debug('Swapping: %s and %s, result %s', first, second, new_order)

        return VisitOrder(genotype.cities, new_order)

