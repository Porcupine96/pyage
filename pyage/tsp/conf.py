# coding=utf-8
# pylint: disable=invalid-name,redefined-outer-name,unnecessary-lambda,unbalanced-tuple-unpacking, redefined-builtin

import sys
import csv
import logging

from pyage.core import address
from pyage.core.agent.aggregate import AggregateAgent
from pyage.core.emas import EmasService
from pyage.core.locator import GridLocator
from pyage.core.migration import ParentMigration
from pyage.core.stats.gnuplot import StepStatistics
from pyage.core.stop_condition import StepLimitStopCondition
from pyage.core.agent.agent import generate_agents, Agent
from pyage.tsp.el_crossover import Crossover
from pyage.tsp.el_init import root_agents_factory, EmasInitializer
from pyage.tsp.el_eval import Evaluator
from pyage.tsp.el_mutation import RandomSwap, NeighbourSwap
from pyage.tsp.el_selection import TournamentSelection
from pyage.tsp.naming_service import NamingService
from pyage.tsp.el_init import SimpleInitializer
from pyage.tsp.el_genotype import City
from pyage.tsp.generator import generate_cities

logger = logging.getLogger(__name__)

args = sys.argv[2:]

if len(args) != 6:
    logger.debug('DEFAULT CONFIG USED!')
    emas = True
    generate_cities = False
    mutation_prob = 0.05
    mutation_type = 'neighbour_swap'
    agents_count = 10
    id = 'default'
else:
    emas = bool(int(args[0]))
    generate_data = bool(int(args[1]))
    mutation_prob = float(args[2])
    mutation_type = args[3]
    agents_count = int(args[4])
    id = args[5]

logger.debug(
    '''
    Running with configuration:
    emas:            %s
    generate_data:   %s
    mutation_prob:   %s
    mutation_type:   %s
    agents_count:    %s
    ''',
    emas,
    generate_data,
    mutation_prob,
    mutation_type,
    agents_count
)

def read_cities(filename):
    cities = []
    with open(filename, 'r') as file:
        reader = csv.reader(file, delimiter=',')
        file.readline()
        for row in reader:
            x, y = row
            cities.append(City(int(x), int(y)))
    return cities

if generate_data:
    cities = generate_cities(25)
else:
    cities = read_cities('cities.csv')

city_count = len(cities)

if emas:
    minimal_energy = lambda: 10
    reproduction_minimum = lambda: 100
    migration_minimum = lambda: 120
    newborn_energy = lambda: 100
    transferred_energy = lambda: 40

    emas = EmasService
    agents = root_agents_factory(agents_count, AggregateAgent)
    aggregated_agents = EmasInitializer(cities, size=40, energy=40)

    evaluation = lambda: Evaluator()
    crossover = lambda: Crossover(city_count)
    mutation = lambda: NeighbourSwap(mutation_prob) if mutation_type == 'neighbour_swap' \
                                                    else RandomSwap(mutation_prob)

else:
    population_size = 10000
    agents = generate_agents('agent', agents_count, Agent)

    operators = lambda: [Evaluator(),
                         TournamentSelection(size=125, tournament_size=125),
                         Crossover(size=130),
                         RandomSwap(mutation_prob)]

    initializer = lambda: SimpleInitializer(cities, population_size)

address_provider = address.SequenceAddressProvider
migration = ParentMigration
locator = GridLocator

stop_condition = lambda: StepLimitStopCondition(2000)
stats = lambda: StepStatistics('fitness_{0}_pyage.txt'.format(id))
naming_service = lambda: NamingService(starting_number=1)
