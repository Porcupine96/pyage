# pylint: disable=invalid-name

import logging
from pyage.core.operator import Operator
from pyage.tsp.el_genotype import VisitOrder

logger = logging.getLogger(__name__)

class Evaluator(Operator):
    def __init__(self):
        super(Evaluator, self).__init__(VisitOrder)

    def process(self, population):
        for genotype in population:
            genotype.fitness = genotype.calculate_fitness()
