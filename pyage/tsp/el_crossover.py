# pylint: disable=invalid-name, redefined-builtin

import logging
from random import sample, randint
from pyage.core.operator import Operator
from pyage.tsp.el_genotype import VisitOrder

logger = logging.getLogger(__name__)

class AbstractCrossover(Operator):
    def __init__(self, type, size):
        super(AbstractCrossover, self).__init__(type)
        self.__size = size

    def cross(self, p1, p2):
        pass

    def process(self, population):
        parents = list(population)
        for _ in range(len(population), self.__size):
            p1, p2 = sample(parents, 2)
            genotype = self.cross(p1, p2)
            population.append(genotype)

class Crossover(AbstractCrossover):
    def __init__(self, size):
        super(Crossover, self).__init__(VisitOrder, size)

    def cross(self, p1, p2):
        logger.debug('Crossing: %s and %s', p1, p2)

        begin, end = Crossover.__split_indices(p1.size)

        path_one = p1.order[:][begin:end]
        path_two = [o for o in p2.order[:] if o not in path_one]

        child = VisitOrder(p1.cities, path_one + path_two)

        logger.debug('Created child: %s', child)

        return child

    @staticmethod
    def __split_indices(size):
        a = randint(0, size-1)
        b = randint(0, size-1)
        return min(a, b), max(a, b)
